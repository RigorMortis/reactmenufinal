
import React, {useEffect, useState} from 'react';

import { Table } from 'react-bootstrap'
import noteService from './services/notes'

const List = () => {
    const [notes, setNotes] = useState([])

    //Haetaan data palvelimelta
    useEffect(() => {
        noteService
            .getAll()
            .then(initialNotes => {
                setNotes(initialNotes)
            })
    }, [])

    return(
        <div className="container">
            {/* A JSX comment /}

            {/ <div>
                <p>Valinta 2.</p>
            </div>
            */}
            <Table striped>
                <tbody>
                <th>Note</th>
                <th>Date</th>
                <tr>
                    <td>
                        {notes.map((note, i) =>
                            <p key={i}>{`${note.content}`}</p>
                        )}
                    </td>
                    <td>
                        {notes.map((note, i) =>
                            <p key={i}>{`${note.date}`}</p>
                        )}
                    </td>
                </tr>
                </tbody>
            </Table>

        </div>
    )
}


export default List