import React, {useEffect, useState} from 'react';
import noteService from './services/notes'
import NoteForm from './NoteForm'
import Notification from './Notification'


const Add = () => {

    const [notes, setNotes] = useState([])
    const [message, setMessage] = useState(null)
    const [newContent, setNewContent ] = useState('')
    const [newDate, setNewDate] = useState('')
    //Lisää henkilö (+ ehdot onnistuneeseen lisäykseen)
    const addNote = (event) =>{
        event.preventDefault()
        const noteObject = {content: newContent, date: newDate}
        if(noteObject.content == ""|| noteObject.date == "" ){
            setMessage(`Urpo täytä kentät!`)
            setTimeout(() => {
                setMessage(null)
            }, 3000)
        }else {
            noteService
                .create(noteObject)
                .then(returnedNote => {
                    setMessage(`Hyvää työtä urpo!`)
                    setTimeout(() => {
                        setMessage(null)
                    }, 3000)
                    setNotes(notes.concat(returnedNote))
                    setNewContent('')
                    setNewDate('')

                })
        }
        setTimeout(() => {

        }, 4000)

    }

    //Synkronoi syötekenttiin tehdyt muutokset
    const handleContentChange= (event) =>{
        setNewContent(event.target.value)
    }

    const handleDateChange = (event) =>{
        setNewDate(event.target.value)
    }

    return(
        <div className="container">
            <div>
                <Notification message={message}/>
                <h4>Uusi</h4>
                <div>
                    <NoteForm add ={addNote} content={newContent} date={newDate} handleContent={handleContentChange} handleDate={handleDateChange}/>
                </div>
            </div>
        </div>
    )
}

export default Add