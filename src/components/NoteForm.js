import React from 'react'

const NoteForm = props => {
    return(
        <form onSubmit={props.add}>
            <label> content: </label>
            <input value={props.content}
                   onChange={props.handleContent}/>

            <label>date:</label>
            <input value={props.date}
                   onChange={props.handleDate}/>
            <b></b>
            <button type="submit">add</button>
        </form>
    )
}

export default NoteForm
